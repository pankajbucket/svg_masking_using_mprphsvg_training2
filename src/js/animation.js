'use strict';
~ function () {
    var $ = TweenMax;

    window.init = function () {
 
        MorphSVGPlugin.convertToPath("rect");
        MorphSVGPlugin.convertToPath("circle");
        // MorphSVGPlugin.convertToPath("polygon");

        var tl = new TimelineMax();
        tl.to("#wave1",2,{morphSVG: "#wave2", ease: Elastic.easeOut},"+=0")
        tl.to("#wave1",2,{morphSVG: "#wave3", ease: Elastic.easeOut},"+=0")
        tl.to("#wave1",1,{morphSVG: "#wave4", ease: Elastic.easeOut},"+=0")
        tl.to("#wave1",.5,{morphSVG: "#wave5", ease: Elastic.easeOut},"+=0")

        tl.set("#circBg",{ opacity: 1 })
        tl.to("#circle1",2,{morphSVG: "#circle2", ease: Elastic.easeOut},"+=0")
    }
   
}();
